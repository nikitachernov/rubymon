module OmniAuthUserFactory
  def self.create(auth)
    User.find_or_create_by(
      provider: auth[:provider],
      uid: auth[:uid]
    )
  end
end
