class UserMonsterFactory
  attr_reader :user, :params

  def initialize(user, params: {})
    @user = user
    @params = params
  end

  def build
    user.monsters.build(monster_params)
  end

  def update_attributes(monster)
    monster.update_attributes(monster_params)
  end

  private

  def monster_params
    params.require(:monster).permit(:name, :power, :kind)
  end
end
