class UserTeamFactory
  attr_reader :user, :params

  def initialize(user, params: {})
    @user = user
    @params = params
  end

  def build
    user.teams.build(team_params)
  end

  def update_attributes(team)
    team.update_attributes(team_params)
  end

  private

  def team_params
    params.require(:team).permit(:name, monster_ids: [])
  end
end
