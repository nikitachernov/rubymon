class MonsterViewModel < ViewModel
  delegate :name, :power, to: :object

  def kind
    object.kind.humanize
  end

  def weakness
    object.weakness.humanize
  end
end
