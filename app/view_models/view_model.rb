class ViewModel
  attr_reader :object

  delegate :to_model, :to_param, to: :object

  def initialize(object)
    @object = object
  end
end
