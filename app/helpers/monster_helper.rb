module MonsterHelper
  def monster_kinds_for_select
    Monster.kinds.keys.map { |kind| [kind.humanize, kind] }
  end
end
