module OmniAuthHelper
  def facebook_login_url
    [
      'https://www.facebook.com/dialog/oauth',
      "?client_id=#{OmniAuth::FACEBOOK_KEY}",
      "&redirect_uri=#{auth_provider_callback_url('facebook')}"
    ].join
  end
end
