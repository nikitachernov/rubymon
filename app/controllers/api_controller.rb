class ApiController < ApplicationController
  before_action :authenticate!

  private

  def authenticate!
    head :unauthorized unless current_user
  end

  def current_user
    token = request.headers['USER-TOKEN']
    return nil if token.blank?
    @current_user ||= User.where(token: token).first
  end
end
