class SessionsController < ApplicationController
  def new
  end

  def create
    user = OmniAuthUserFactory.create(auth_hash)
    session[:user_id] = user.id
    redirect_to root_url
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
