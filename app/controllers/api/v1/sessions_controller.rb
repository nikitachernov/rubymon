module Api
  module V1
    class SessionsController < ApplicationController
      def create
        token = params[:oauth_token]
        facebook_user_info = FacebookUserInfoService.new(token).get

        user = OmniAuthUserFactory.create(
          uid: facebook_user_info['id'],
          provider: 'facebook'
        )

        render json: { token: user.token }
      end
    end
  end
end
