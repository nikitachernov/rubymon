module Api
  module V1
    class TeamsController < ApiController
      before_action :find_team, only: [:show, :update, :destroy]

      def index
        @teams = UserTeamsQuery.new(current_user).all
      end

      def show
      end

      def create
        @team = UserTeamFactory.new(current_user, params: params).build

        if @team.save
          render @team
        else
          render json: @team.errors
        end
      end

      def update
        user_team_factory = UserTeamFactory.new(current_user, params: params)
        if user_team_factory.update_attributes(@team)
          render @team
        else
          render json: @team.errors
        end
      end

      def destroy
        @team.destroy
        head :ok
      end

      private

      def find_team
        @team = UserTeamQuery.new(current_user).find(params[:id])
      end
    end
  end
end
