module Api
  module V1
    class MonstersController < ApiController
      before_action :find_monster, only: [:show, :update, :destroy]

      def index
        @monsters = UserMonstersQuery.new(current_user).all
      end

      def show
      end

      def create
        @monster = UserMonsterFactory.new(current_user, params: params).build

        if @monster.save
          render @monster
        else
          render json: @monster.errors
        end
      end

      def update
        user_monster_factory = UserMonsterFactory.new(
          current_user, params: params
        )
        if user_monster_factory.update_attributes(@monster)
          render @monster
        else
          render json: @monster.errors
        end
      end

      def destroy
        @monster.destroy
        head :ok
      end

      private

      def find_monster
        @monster = UserMonsterQuery.new(current_user).find(params[:id])
      end
    end
  end
end
