class WebController < ApplicationController
  before_action :authenticate!

  private

  def authenticate!
    redirect_to signin_url unless current_user
  end

  def current_user
    return nil if session[:user_id].blank?
    @current_user ||= User.find(session[:user_id])
  end

  helper_method :current_user
end
