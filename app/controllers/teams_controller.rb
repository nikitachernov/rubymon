class TeamsController < WebController
  before_action :find_team, only: [:show, :edit, :update, :destroy]
  before_action :find_monsters, only: [:new, :edit]

  def index
    @teams = UserTeamsQuery.new(current_user).to_a
  end

  def show
  end

  def new
    @team = Team.new
  end

  def create
    @team = UserTeamFactory.new(current_user, params: params).build

    if @team.save
      flash[:notice] = I18n.t('team.created')
      redirect_to @team
    else
      flash.now[:error] = I18n.t('team.error')
      find_monsters
      render :new
    end
  end

  def edit
  end

  def update
    user_team_factory = UserTeamFactory.new(current_user, params: params)
    if user_team_factory.update_attributes(@team)
      flash[:notice] = I18n.t('team.updated')
      redirect_to @team
    else
      flash.now[:eror] = I18n.t('team.error')
      find_monsters
      render :edit
    end
  end

  def destroy
    @team.destroy
    flash[:notice] = I18n.t('team.destroyed')
    redirect_to teams_url
  end

  private

  def find_team
    @team = UserTeamQuery.new(current_user).find(params[:id])
  end

  def find_monsters
    @monsters = UserMonstersQuery.new(current_user, params: params).to_a
  end
end
