class MonstersController < WebController
  before_action :find_monster, only: [:show, :edit, :update, :destroy]

  def index
    query = UserMonstersQuery.new(current_user, params: params)
    @monsters = query.map do |monster|
      MonsterViewModel.new(monster)
    end
  end

  def show
    @monster = MonsterViewModel.new(@monster)
  end

  def new
    @monster = Monster.new
  end

  def create
    @monster = UserMonsterFactory.new(current_user, params: params).build

    if @monster.save
      flash[:notice] = I18n.t('monster.created')
      redirect_to @monster
    else
      flash.now[:error] = I18n.t('monster.error')
      render :new
    end
  end

  def edit
  end

  def update
    user_monster_factory = UserMonsterFactory.new(current_user, params: params)
    if user_monster_factory.update_attributes(@monster)
      flash[:notice] = I18n.t('monster.updated')
      redirect_to @monster
    else
      flash.now[:eror] = I18n.t('monster.error')
      render :edit
    end
  end

  def destroy
    @monster.destroy
    flash[:notice] = I18n.t('monster.destroyed')
    redirect_to monsters_url
  end

  private

  def find_monster
    @monster = UserMonsterQuery.new(current_user).find(params[:id])
  end
end
