class UserTeamsQuery < Query
  attr_reader :user, :params

  def initialize(user)
    @user = user
    @params = params
  end

  private

  def criteria
    user.teams.order(:name)
  end

  def order_params
    if params[:order] && [:name, :power, :kind].include?(params[:order].to_sym)
      params[:order]
    else
      :name
    end
  end
end
