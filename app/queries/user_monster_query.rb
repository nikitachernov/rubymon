class UserMonsterQuery < Query
  attr_reader :user

  def initialize(user)
    @user = user
  end

  private

  def criteria
    user.monsters
  end
end
