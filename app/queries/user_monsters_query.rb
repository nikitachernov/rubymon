class UserMonstersQuery < Query
  attr_reader :user, :params

  def initialize(user, params: {})
    @user = user
    @params = params
  end

  private

  def criteria
    user.monsters.order(order_params)
  end

  def order_params
    if params[:order] && [:name, :power, :kind].include?(params[:order].to_sym)
      params[:order]
    else
      :name
    end
  end
end
