class Query
  def all
    criteria.all
  end

  def to_a
    criteria.to_a
  end

  def map(&block)
    criteria.map(&block)
  end

  def find(id)
    criteria.find(id)
  end

  private

  def criteria
    raise NotImplementedError
  end
end
