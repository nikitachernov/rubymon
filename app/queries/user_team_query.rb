class UserTeamQuery < Query
  attr_reader :user

  def initialize(user)
    @user = user
  end

  private

  def criteria
    user.teams.includes(:monsters)
  end
end
