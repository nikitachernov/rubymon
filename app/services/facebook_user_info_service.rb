class FacebookUserInfoService
  attr_reader :token

  def initialize(token)
    @token = token
  end

  def get
    client.get_object('me', fields: ['id'])
  end

  private

  def client
    @client ||= Koala::Facebook::API.new(token)
  end
end
