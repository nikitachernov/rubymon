class Team < ApplicationRecord
  belongs_to :user
  has_many :monsters

  validates :name, presence: true, uniqueness: { scope: :user_id }
  validates :monsters, length: { minimum: 0, maximum: 3 }
end
