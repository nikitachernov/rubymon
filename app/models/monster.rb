class Monster < ApplicationRecord
  validates :name, :power, :kind, presence: true

  enum kind: [:fire, :water, :earth, :electric, :wind]

  validates :name, uniqueness: { scope: :user }

  belongs_to :user
  belongs_to :team, required: false

  validate :user_must_match_team_user

  def weakness
    next_kind_index = (Monster.kinds[kind] + 1) % Monster.kinds.size
    Monster.kinds.invert[next_kind_index]
  end

  private

  def user_must_match_team_user
    return true if team.blank? || user == team.user
    errors.add(:user, I18n.t('monster.user_must_match_team_user'))
  end
end
