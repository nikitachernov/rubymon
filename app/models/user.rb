class User < ApplicationRecord
  MONSTER_LIMIT = 20
  TEAM_LIMIT = 3

  has_secure_token

  validates :uid, :provider, presence: true
  validates :uid, uniqueness: { scope: :provider }

  has_many :monsters, dependent: :destroy
  has_many :teams, dependent: :destroy

  validates :monsters, length: { minimum: 0, maximum: MONSTER_LIMIT }
  validates :teams, length: { minimum: 0, maximum: TEAM_LIMIT }
end
