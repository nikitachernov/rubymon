(function() {
  this.App || (this.App = {});

  App.TeamFormValidator = class TeamFormValidator {
    constructor() {
      this.form = "form[data-role=team-form]";
      this.monsterIds = `input[name='team[monster_ids][]']:not([type=hidden])`;
      this.submitButton = "input[type=submit]";
    }

    bindValidation() {
      $(this.form).on("change", this.monsterIds, () => {
        this.validate();
      });
    }

    validate() {
      this.validateMonsterIds();
    }

    validateMonsterIds() {
      const $monsterIds = $(this.monsterIds);
      const $checkedMonsterids = $(`${this.monsterIds}:checked`);
      const $notCheckedMonsterids = $(`${this.monsterIds}:not(:checked)`);

      const reachedcheckedMonsterIdsLimit = $checkedMonsterids.length >= 3;

      $notCheckedMonsterids.prop("disabled", reachedcheckedMonsterIdsLimit);
    }
  };
}).call(this);

document.addEventListener("turbolinks:load", function() {
  const teamFormValidator = new App.TeamFormValidator();
  teamFormValidator.bindValidation();
  teamFormValidator.validate();
});
