Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'signin', to: 'sessions#new'

  get(
    '/auth/:provider/callback',
    to: 'sessions#create',
    as: :auth_provider_callback
  )

  resources :monsters
  resources :teams

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :sessions, only: [:create]
      resources :monsters, only: [:index, :show, :create, :update, :destroy]
      resources :teams, only: [:index, :show, :create, :update, :destroy]
    end
  end

  root to: 'monsters#index'
end
