module OmniAuth
  FACEBOOK_KEY = ENV['FACEBOOK_KEY']
  FACEBOOK_SECRET = ENV['FACEBOOK_SECRET']

  Rails.application.config.middleware.use OmniAuth::Builder do
    provider(
      :facebook,
      FACEBOOK_KEY,
      FACEBOOK_SECRET
    )
  end
end
