require 'test_helper'

class FacebookUserInfoServiceTest < ActiveSupport::TestCase
  test 'should get user Facebook ID' do
    VCR.use_cassette('facebook_ouath') do
      token = 'FAKE'
      facebook_user_info_service = FacebookUserInfoService.new(token)
      info = facebook_user_info_service.get
      assert_equal({ 'id' => '1538603475' }, info)
    end
  end
end
