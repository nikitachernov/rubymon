ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara/rails'
require 'vcr'
require 'mocha/test_unit'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  VCR.configure do |config|
    config.cassette_library_dir = 'test/vcr'
    config.hook_into :webmock
  end
end

module LoginHelper
  def login_url
    auth_provider_callback_url(provider: :facebook)
  end
end

module HtmlHelper
  def row(n)
    "tbody tr:nth-child(#{n})"
  end

  def notice_alert
    '[data-role=alert].notice'
  end

  def error_alert
    '[data-role=alert].error'
  end
end

module JSONHelper
  def json_response
    json = JSON.parse(response.body)
    if json.is_a?(Array)
      json.map(&:symbolize_keys)
    else
      json.symbolize_keys
    end
  end
end

class ActionDispatch::IntegrationTest
  include LoginHelper
  include HtmlHelper
  include JSONHelper

  include Capybara::DSL

  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end
