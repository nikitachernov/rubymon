require 'test_helper'

module Api
  module V1
    class MonstersControllerTest < ActionDispatch::IntegrationTest
      attr_reader :monster, :headers

      setup do
        @monster = monsters(:one)
        @headers = { 'USER-TOKEN' => 'xyz' }
      end

      test 'should get index' do
        get api_v1_monsters_url, headers: headers
        assert_response :success
        assert_equal(
          [
            { id: 3, name: 'Bull', power: 'Leaf', kind: 'earth' },
            { id: 2, name: 'Chary', power: 'ZigZag', kind: 'fire' },
            { id: 1, name: 'Picka', power: 'Thunderstrike', kind: 'electric' }
          ],
          json_response
        )
      end

      test 'should show monster' do
        get api_v1_monster_url(monster), headers: headers
        assert_response :success
        assert_equal(
          { id: 1, name: 'Picka', power: 'Thunderstrike', kind: 'electric' },
          json_response
        )
      end

      test 'should not show monster from other user' do
        other_monster = users(:two).monsters.first

        assert_raises(ActiveRecord::RecordNotFound) do
          get(api_v1_monster_url(other_monster), headers: headers)
        end
      end

      test 'should create monster' do
        assert_difference('Monster.count') do
          post(
            api_v1_monsters_url,
            params: { monster: {
              name: 'Seal', power: 'Watergun', kind: 'water'
            } },
            headers: headers
          )
        end

        assert_equal(
          {
            id: Monster.last.id, name: 'Seal', power: 'Watergun', kind: 'water'
          },
          json_response
        )
      end

      test 'should not create invalid monster' do
        assert_no_difference('Monster.count') do
          post(
            api_v1_monsters_url,
            params: { monster: { name: '', power: '', kind: '' } },
            headers: headers
          )
        end

        assert_equal(
          {
            name: ["can't be blank"],
            power: ["can't be blank"],
            kind: ["can't be blank"]
          },
          json_response
        )
      end

      test 'should destroy monster' do
        assert_difference('Monster.count', -1) do
          delete(api_v1_monster_url(monster), headers: headers)
        end

        assert_response :ok
      end

      test 'should not destroy monster from other user' do
        other_monster = users(:two).monsters.first

        assert_raises(ActiveRecord::RecordNotFound) do
          delete(api_v1_monster_url(other_monster), headers: headers)
        end
      end

      test 'should update monster' do
        patch(
          api_v1_monster_url(monster),
          params: { monster: { name: 'Richu' } },
          headers: headers
        )

        monster.reload
        assert_equal 'Richu', monster.name
        assert_equal(
          { id: 1, name: 'Richu', power: 'Thunderstrike', kind: 'electric' },
          json_response
        )
      end

      test 'should not update monster from other user' do
        other_monster = users(:two).monsters.first

        assert_raises(ActiveRecord::RecordNotFound) do
          patch(
            api_v1_monster_url(other_monster),
            params: { monster: { name: 'Richu' } },
            headers: headers
          )
        end
      end
    end
  end
end
