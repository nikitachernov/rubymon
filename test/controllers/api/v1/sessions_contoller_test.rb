require 'test_helper'

module Api
  module V1
    class SessionsControllerTest < ActionDispatch::IntegrationTest
      attr_reader :token, :facebook_user_info_service

      setup do
        @token = 'fb_xyz'
        @facebook_user_info_service = FacebookUserInfoService.new(token)
      end

      test 'authorize authorize user by FB access token' do
        FacebookUserInfoService.expects(:new).with(token).returns(
          facebook_user_info_service
        )

        facebook_user_info_service.expects(:get).returns('id' => '123')

        post api_v1_sessions_url(provider: 'facebook', oauth_token: token)

        assert_equal({ token: 'xyz' }, json_response)
      end

      def teardown
        FacebookUserInfoService.unstub(:new)
      end
    end
  end
end
