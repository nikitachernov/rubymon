require 'test_helper'

module Api
  module V1
    class TeamsControllerTest < ActionDispatch::IntegrationTest
      attr_reader :team, :headers

      setup do
        @team = teams(:one)
        @headers = { 'USER-TOKEN' => 'xyz' }
      end

      test 'should get index' do
        get api_v1_teams_url, headers: headers
        assert_response :success
        assert_equal(
          [{ id: 1, name: 'Team Shuttle', monster_ids: [1, 2] }],
          json_response
        )
      end

      test 'should show team' do
        get api_v1_team_url(team), headers: headers
        assert_response :success
        assert_equal(
          { id: 1, name: 'Team Shuttle', monster_ids: [1, 2] }, json_response
        )
      end

      test 'should not show team from other user' do
        other_team = users(:two).teams.first

        assert_raises(ActiveRecord::RecordNotFound) do
          get(api_v1_team_url(other_team), headers: headers)
        end
      end

      test 'should create team' do
        assert_difference('Team.count') do
          post(
            api_v1_teams_url,
            params: { team: { name: 'Agua', monster_ids: [1, 2, 3] } },
            headers: headers
          )
        end

        assert_equal(
          { id: Team.last.id, name: 'Agua', monster_ids: [1, 2, 3] },
          json_response
        )
      end

      test 'should not create invalid team' do
        assert_no_difference('Team.count') do
          post(
            api_v1_teams_url,
            params: { team: { name: '', monster_ids: [1, 2, 3] } },
            headers: headers
          )
        end

        assert_equal({ name: ["can't be blank"] }, json_response)
      end

      test 'should destroy team' do
        assert_difference('Team.count', -1) do
          delete(api_v1_team_url(team), headers: headers)
        end

        assert_response :ok
      end

      test 'should not destroy team from other user' do
        other_team = users(:two).teams.first

        assert_raises(ActiveRecord::RecordNotFound) do
          delete(api_v1_team_url(other_team), headers: headers)
        end
      end

      test 'should update team' do
        patch(
          api_v1_team_url(team),
          params: { team: { name: 'Team Lava' } },
          headers: headers
        )

        team.reload
        assert_equal 'Team Lava', team.name
        assert_equal(
          { id: 1, name: 'Team Lava', monster_ids: [1, 2] }, json_response
        )
      end

      test 'should not update team from other user' do
        other_team = users(:two).teams.first

        assert_raises(ActiveRecord::RecordNotFound) do
          patch(
            api_v1_team_url(other_team),
            params: { team: { name: 'Richu' } },
            headers: headers
          )
        end
      end
    end
  end
end
