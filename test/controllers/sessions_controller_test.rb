require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    User.destroy_all
  end

  test 'should redirect to root' do
    get login_url
    assert_redirected_to root_url
  end

  test 'should write user id to session' do
    get login_url
    assert_equal User.last.id, session[:user_id]
  end

  test 'should create user' do
    get login_url
    assert_equal 1, User.count
  end

  test 'should not create same user twice' do
    get login_url
    get login_url
    assert_equal 1, User.count
  end
end
