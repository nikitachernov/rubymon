require 'test_helper'

class TeamsControllerTest < ActionDispatch::IntegrationTest
  attr_reader :team

  setup do
    @team = teams(:one)
    get login_url
  end

  test 'should get index' do
    get teams_url
    assert_response :success
  end

  test 'should show team' do
    get team_url(team)
    assert_response :success
  end

  test 'should not show team from other user' do
    other_team = users(:two).teams.first

    assert_raises(ActiveRecord::RecordNotFound) do
      get team_url(other_team)
    end
  end

  test 'should create team' do
    assert_difference('Team.count') do
      post teams_url, params: { team: {
        name: 'Agua'
      } }
    end

    assert_redirected_to team_url(Team.last)
  end

  test 'should not create invalid team' do
    assert_no_difference('Team.count') do
      post teams_url, params: { team: {
        name: ''
      } }
    end
  end

  test 'should destroy team' do
    assert_difference('Team.count', -1) do
      delete team_url(team)
    end

    assert_redirected_to teams_url
  end

  test 'should not destroy team from other user' do
    other_team = users(:two).teams.first

    assert_raises(ActiveRecord::RecordNotFound) do
      delete team_url(other_team)
    end
  end

  test 'should update team' do
    patch team_url(team), params: { team: { name: 'Lava' } }

    assert_redirected_to team_url(team)
    team.reload
    assert_equal 'Lava', team.name
  end

  test 'should not update team from other user' do
    other_team = users(:two).teams.first

    assert_raises(ActiveRecord::RecordNotFound) do
      patch team_url(other_team), params: { team: { name: 'Lava' } }
    end
  end
end
