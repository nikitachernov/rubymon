require 'test_helper'

class MonstersControllerTest < ActionDispatch::IntegrationTest
  attr_reader :monster

  setup do
    @monster = monsters(:one)
    get login_url
  end

  test 'should get index' do
    get monsters_url
    assert_response :success
  end

  test 'should show monster' do
    get monster_url(monster)
    assert_response :success
  end

  test 'should not show monster from other user' do
    other_monster = users(:two).monsters.first

    assert_raises(ActiveRecord::RecordNotFound) do
      get monster_url(other_monster)
    end
  end

  test 'should create monster' do
    assert_difference('Monster.count') do
      post monsters_url, params: { monster: {
        name: 'Seal', power: 'Watergun', kind: 'water'
      } }
    end

    assert_redirected_to monster_url(Monster.last)
  end

  test 'should not create invalid monster' do
    assert_no_difference('Monster.count') do
      post monsters_url, params: { monster: {
        name: 'Seal'
      } }
    end
  end

  test 'should destroy monster' do
    assert_difference('Monster.count', -1) do
      delete monster_url(monster)
    end

    assert_redirected_to monsters_url
  end

  test 'should not destroy monster from other user' do
    other_monster = users(:two).monsters.first

    assert_raises(ActiveRecord::RecordNotFound) do
      delete monster_url(other_monster)
    end
  end

  test 'should update monster' do
    patch monster_url(monster), params: { monster: { name: 'Richu' } }

    assert_redirected_to monster_url(monster)
    monster.reload
    assert_equal 'Richu', monster.name
  end

  test 'should not update monster from other user' do
    other_monster = users(:two).monsters.first

    assert_raises(ActiveRecord::RecordNotFound) do
      patch monster_url(other_monster), params: { monster: { name: 'Richu' } }
    end
  end
end
