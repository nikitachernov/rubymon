require 'test_helper'

class UserTeamFactoryTest < ActiveSupport::TestCase
  attr_reader :user, :monster, :params

  setup do
    @user = users(:one)
    @monster = monsters(:one)

    @params = ActionController::Parameters.new(
      team: {
        name: 'Team A',
        monster_ids: [monster.id]
      }
    )
  end

  test 'should build new team for user' do
    user_team_factory = UserTeamFactory.new(user, params: params)
    team = user_team_factory.build

    assert team.valid?
  end

  test 'should update team for user' do
    user_team_factory = UserTeamFactory.new(user, params: params)
    team = user_team_factory.build
    team.save

    assert_equal 'Team A', team.name
    assert team.monsters.present?
    assert team.persisted?

    params[:team][:name] = 'Team B'
    params[:team][:monster_ids] = []
    user_team_factory.update_attributes(team)

    assert_equal 'Team B', team.name
    assert team.monsters.blank?
    assert team.persisted?
  end
end
