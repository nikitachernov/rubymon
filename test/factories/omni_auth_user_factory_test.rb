require 'test_helper'

class OmniAuthUserFactoryTest < ActiveSupport::TestCase
  test 'should create user by uid and provider' do
    user = OmniAuthUserFactory.create(
      auth(provider: 'facebook', uid: '1234')
    )

    assert_equal user, User.last
    assert_equal 'facebook', user.provider
    assert_equal '1234', user.uid
  end

  test 'should not create create duplicated user' do
    user_count = User.count
    OmniAuthUserFactory.create(auth)
    OmniAuthUserFactory.create(auth)
    assert_equal user_count + 1, User.count
  end

  test 'should create new user with diffent uid' do
    user_count = User.count
    OmniAuthUserFactory.create(auth)
    OmniAuthUserFactory.create(auth(uid: '4321'))
    assert_equal user_count + 2, User.count
  end

  test 'should create new user with diffent provider' do
    user_count = User.count
    OmniAuthUserFactory.create(auth)
    OmniAuthUserFactory.create(auth(provider: 'twitter'))
    assert_equal user_count + 2, User.count
  end

  def auth(provider: 'facebook', uid: '1234')
    OmniAuth::AuthHash.new(provider: provider, uid: uid)
  end
end
