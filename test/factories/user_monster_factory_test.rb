require 'test_helper'

class UserMonsterFactoryTest < ActiveSupport::TestCase
  attr_reader :user, :params

  setup do
    @user = users(:one)
    @params = ActionController::Parameters.new(
      monster: {
        name: 'Dragon',
        power: 'Fireball',
        kind: 'fire'
      }
    )
  end

  test 'should build new monster for user' do
    user_monster_factory = UserMonsterFactory.new(user, params: params)
    monster = user_monster_factory.build

    assert monster.valid?
  end

  test 'should update monster for user' do
    user_monster_factory = UserMonsterFactory.new(user, params: params)
    monster = user_monster_factory.build
    monster.save

    assert_equal 'Dragon', monster.name
    assert monster.persisted?

    params[:monster][:name] = 'Butterfly'
    params[:monster][:power] = 'Sprinkle'
    params[:monster][:kind] = 'wind'
    user_monster_factory.update_attributes(monster)

    assert_equal 'Butterfly', monster.name
    assert_equal 'Sprinkle', monster.power
    assert_equal 'wind', monster.kind
    assert monster.persisted?
  end
end
