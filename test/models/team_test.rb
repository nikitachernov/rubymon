require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  attr_reader :team

  setup do
    @team = teams(:one)
  end

  test 'team is valid' do
    assert team.valid?
  end

  test 'not valid without name' do
    team.name = nil
    assert team.invalid?
  end

  test 'not valid without uniq name for the same user' do
    new_team = Team.new(team.attributes)
    assert new_team.invalid?
  end

  test 'valid without uniq name for different user' do
    user = users(:two)
    new_team = Team.new(team.attributes.merge(user: user))
    assert new_team.valid?
  end

  test 'not valid with 4 monsters' do
    2.times do |i|
      team.monsters.create!(
        name: i,
        power: 'Flamethrower',
        kind: 'fire',
        user: team.user
      )
    end

    assert team.invalid?
  end
end
