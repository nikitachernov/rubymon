require 'test_helper'

class MonsterTest < ActiveSupport::TestCase
  attr_reader :monster

  setup do
    @monster = monsters(:one)
  end

  test 'monster is valid' do
    assert monster.valid?
  end

  test 'not valid without name' do
    monster.name = nil
    assert monster.invalid?
  end

  test 'not valid without power' do
    monster.power = nil
    assert monster.invalid?
  end

  test 'not valid without kind' do
    monster.kind = nil
    assert monster.invalid?
  end

  test 'not valid without user' do
    monster.user = nil
    assert monster.invalid?
  end

  test 'not valid without uniq name for the same user' do
    new_monster = Monster.new(monster.attributes)
    assert new_monster.invalid?
  end

  test 'valid without uniq name for different user' do
    user = users(:two)
    new_monster = Monster.new(
      monster.attributes.merge(team: nil, user: user)
    )

    assert new_monster.valid?
  end

  test 'invalid in the wrong team' do
    monster.user = users(:two)
    assert monster.invalid?
  end

  test 'electric weakness' do
    monster.kind = 'electric'
    assert_equal 'wind', monster.weakness
  end

  test 'wind weakness' do
    monster.kind = 'wind'
    assert_equal 'fire', monster.weakness
  end
end
