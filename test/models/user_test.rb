require 'test_helper'

class UserTest < ActiveSupport::TestCase
  attr_reader :user

  setup do
    @user = users(:one)
  end

  test 'user is valid' do
    assert user.valid?
  end

  test 'not valid without uid' do
    user.uid = nil
    assert user.invalid?
  end

  test 'not valid without provider' do
    user.provider = nil
    assert user.invalid?
  end

  test 'not valid without uniq uid for the same provider' do
    new_user = User.new(user.attributes)
    assert new_user.invalid?
  end

  test 'valid without uniq uid for different provider' do
    new_user = User.new(user.attributes.merge(provider: 'twitter'))
    assert new_user.valid?
  end

  test 'valid with 20 monsters' do
    17.times do |i|
      user.monsters.create(name: i, power: 'fire', kind: 'fire')
    end

    assert user.valid?
  end

  test 'not valid with 21 monsters' do
    18.times do |i|
      user.monsters.create(name: i, power: 'Flamethrower', kind: 'fire')
    end

    assert user.invalid?
  end

  test 'valid with 3 teams' do
    2.times do |i|
      user.teams.create!(name: i)
    end

    assert user.valid?
  end

  test 'not valid with 4 teams' do
    3.times do |i|
      user.teams.create!(name: i)
    end
    user.valid?

    assert user.invalid?
  end
end
