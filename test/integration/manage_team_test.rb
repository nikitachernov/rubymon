require 'test_helper'

class ManageTeamTest < ActionDispatch::IntegrationTest
  setup do
    visit login_url
    click_on 'Teams'
  end

  test 'List teams' do
    assert page.has_content?('Team Shuttle')
    click_link 'Team Shuttle'
    assert page.has_content?('Team Shuttle')
    assert page.has_content?('Picka')
    assert page.has_content?('Chary')
    assert_not page.has_content?('Bull')
  end

  test 'List only teams for user' do
    assert_equal 2, Team.count
    assert_equal 1, page.find_all('tbody tr').length
  end

  test 'Create a team' do
    click_link 'Add a new team'
    fill_in 'Name', with: 'Team Lava'
    click_button 'Create Team'

    within notice_alert do
      assert page.has_content?('Created a new team')
    end

    assert page.has_content?('Team Lava')
  end

  test 'Create invalid team' do
    click_link 'Add a new team'
    click_button 'Create Team'

    within error_alert do
      assert page.has_content?('Team is not valid')
    end
  end

  test 'Should not see a link if to many teams' do
    user = users(:one)

    2.times do |i|
      user.teams.create!(name: i)
    end

    visit teams_url

    assert_equal 3, page.find_all('tbody tr').length
    assert_not page.has_content?('Add a new team')
  end

  test 'Update a team' do
    click_link 'Team Shuttle'
    all(:link, 'Edit').last.click
    fill_in 'Name', with: 'Team Lava'
    check 'Bull'
    click_button 'Update Team'

    within notice_alert do
      assert page.has_content?('Updated a team')
    end

    assert page.has_content?('Team Lava')
    assert page.has_content?('Bull')
  end

  test 'Delete a team' do
    first(:link, 'Delete').click

    within notice_alert do
      assert page.has_content?('Deleted a team')
    end

    assert_not page.has_content?('Team Shuttle')
  end
end
