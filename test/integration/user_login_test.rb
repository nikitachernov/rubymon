require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest
  test 'User logs in' do
    get '/'
    follow_redirect!
    assert_select 'a', 'Login'

    get login_url
    follow_redirect!
    assert_select 'h1', 'Monsters'
  end
end
