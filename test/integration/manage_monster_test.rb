require 'test_helper'

class ManageMonsterTest < ActionDispatch::IntegrationTest
  setup { visit login_url }

  test 'List monsters' do
    within row(1) do
      assert page.has_content?('Bull')
      assert page.has_content?('Leaf')
      assert page.has_content?('Earth')
      assert page.has_content?('Electric')
    end

    within row(2) do
      assert page.has_content?('Chary')
      assert page.has_content?('ZigZag')
      assert page.has_content?('Fire')
      assert page.has_content?('Water')
    end

    within row(3) do
      assert page.has_content?('Picka')
      assert page.has_content?('Thunderstrike')
      assert page.has_content?('Electric')
      assert page.has_content?('Wind')
    end
  end

  test 'Sort monsters' do
    click_link 'Name'
    within(row(1)) { assert page.has_content?('Bull') }
    within(row(2)) { assert page.has_content?('Chary') }
    within(row(3)) { assert page.has_content?('Picka') }

    click_link 'Power'
    within(row(1)) { assert page.has_content?('Bull') }
    within(row(2)) { assert page.has_content?('Picka') }
    within(row(3)) { assert page.has_content?('Chary') }

    click_link 'Type'
    within(row(1)) { assert page.has_content?('Chary') }
    within(row(2)) { assert page.has_content?('Bull') }
    within(row(3)) { assert page.has_content?('Picka') }

    click_link 'Weakness'
    within(row(1)) { assert page.has_content?('Chary') }
    within(row(2)) { assert page.has_content?('Bull') }
    within(row(3)) { assert page.has_content?('Picka') }
  end

  test 'View monster' do
    click_link 'Picka'

    assert_not page.has_content?('Bull')
    assert_not page.has_content?('Chary')

    assert page.has_content?('Picka')
    assert page.has_content?('Thunderstrike')
    assert page.has_content?('Electric')
  end

  test 'List only monsters for user' do
    assert_equal 4, Monster.count
    assert_equal 3, page.find_all('tbody tr').length
  end

  test 'Create a monster' do
    click_link 'Add a new monster'
    fill_in 'Name', with: 'Birdie'
    fill_in 'Power', with: 'Fly'
    select 'Wind', from: 'Type'
    click_button 'Create Monster'

    within(notice_alert) { assert page.has_content?('Created a new monster') }

    assert page.has_content?('Birdie')
    assert page.has_content?('Fly')
    assert page.has_content?('Wind')
  end

  test 'Create invalid monster' do
    click_link 'Add a new monster'
    click_button 'Create Monster'

    within(error_alert) { assert page.has_content?('Monster is not valid') }
  end

  test 'Should not see a link if to many monsters' do
    user = users(:one)

    17.times do |i|
      user.monsters.create!(name: i, power: 'Flamethrower', kind: 'fire')
    end

    visit root_url

    assert_equal 20, page.find_all('tbody tr').length
    assert_not page.has_content?('Add a new monster')
  end

  test 'Update a monster' do
    click_link 'Picka'
    click_link 'Edit'
    fill_in 'Name', with: 'Birdie'
    fill_in 'Power', with: 'Fly'
    select 'Wind', from: 'Type'
    click_button 'Update Monster'

    within(notice_alert) { assert page.has_content?('Updated a monster') }

    assert page.has_content?('Birdie')
    assert page.has_content?('Fly')
    assert page.has_content?('Wind')
    assert page.has_content?('Fire')
  end

  test 'Delete a monster' do
    first(:link, 'Delete').click

    within(notice_alert) { assert page.has_content?('Deleted a monster') }

    assert_not page.has_content?('Bull')

    within(row(1)) { assert page.has_content?('Chary') }
    within(row(2)) { assert page.has_content?('Picka') }
  end
end
