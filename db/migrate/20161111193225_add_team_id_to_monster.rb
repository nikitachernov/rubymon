class AddTeamIdToMonster < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :monsters, :team
  end
end
